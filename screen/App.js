import React, { Component } from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity, Platform, Button } from "react-native";
import { Card } from 'react-native-elements'
import RBSheet from "react-native-raw-bottom-sheet";
import Menu from './components/index'

export default class Example extends Component {
  render() {
    return (
      
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={600}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center"
            }
          }}
        >
        <Menu />
        </RBSheet>
        <View style={{flex: 1, backgroundColor: "#ecf0f1"}}>
          <View style={{height: 60, backgroundColor: "#44bd32", flexDirection: "row", alignItems: 'center', justifyContent: 'center'}}>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 26, height: 26}} source={require('./assets/icons/promo.png')} />
              <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Life</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 26, height: 26}} source={require('./assets/icons/home.png')} />
              <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Home</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 26, height: 26}} source={require('./assets/icons/setting.png')} />
              <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Setting</Text>
            </TouchableOpacity>
          </View>
      <Card>
        <Card.Image source={require('./assets/images/home-1.png')} />
        <Text style={{marginBottom: 5, marginTop: 5}}>
            The idea with React Native Elements is more about component structure than actual design.
        </Text>
      </Card>
      <Card>
        <Card.Image source={require('./assets/images/home-1.png')} />
        <Text style={{marginBottom: 5, marginTop: 5}}>
            The idea with React Native Elements is more about component structure than actual design.
        </Text>
      </Card>
      </View>
      <View style={{height: 60, backgroundColor: "#44bd32", flexDirection: "row", alignItems: 'center', justifyContent: 'center'}}>
      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()}>
      <Image style={{ width: 26, height: 26}} source={require('./assets/icons/notif.png')} />
        <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Notification</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()}>
      <Image style={{ width: 26, height: 26}} source={require('./assets/icons/order.png')} />
        <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Order</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()}>
      <Image style={{ width: 26, height: 26}} source={require('./assets/icons/inbox.png')} />
        <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Inbox</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()}>
      <Image style={{ width: 26, height: 26}} source={require('./assets/icons/account.png')} />
        <Text style={{fontSize: 10, color: '#ecf0f1', marginTop: 4}}>Account</Text>
      </TouchableOpacity>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});