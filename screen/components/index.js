import React, { Component } from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity, Platform, Button } from "react-native";
import { Card } from 'react-native-elements'
import RBSheet from "react-native-raw-bottom-sheet";

export default class Example extends Component {
  render() {
    return (
      
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={600}
          openDuration={250}
          customStyles={{
            container: {
              justifyContent: "center",
              alignItems: "center"
            }
          }}
        >
        <Text>Hallo</Text>
        </RBSheet>
        <View style={{flex: 1, backgroundColor: "#ecf0f1"}}>
          <View style={{height: 60, flexDirection: "row", alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/1.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/2.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/3.png')} />
            </TouchableOpacity>
          </View>
          <View style={{height: 60, flexDirection: "row", alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/4.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/5.png')} />
            </TouchableOpacity>
            <TouchableOpacity style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} onPress={() => this.RBSheet.open()} >
            <Image style={{ width: 40, height: 40}} source={require('../assets/icons/6.png')} />
            </TouchableOpacity>
          </View>
      <Card>
        <Card.Image source={require('../assets/images/home-3.png')} />
        <Text style={{marginBottom: 5, marginTop: 5}}>
            The idea with React Native Elements is more about component structure than actual design.
        </Text>
      </Card>
      </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});